const {MongoClient} = require('mongodb')
const url = 'mongodb://localhost:27017/test'


//connecting to mongodb
// MongoClient.connect(url, (err, client) => {
//     if (err) {
//         console.log('couldnt connect')
//     }
//     console.log('connected')
//
//     client.close()
// })


//inserting data

// MongoClient.connect(url, (err, client) => {
//     const db = client.db()
//     db.collection('cars').insertOne({
//         model: 'Ford',
//         year: 2018
//     }, (err, res) => {
//         if (err) throw err;
//         console.log(res.ops + " " + "timeFromId: " + res.ops[0]._id.getTimestamp())
//     })
//     client.close()
// })


//inserting multiple data

// MongoClient.connect(url, (err, client) => {
//     const db = client.db()
//     const cars = [{
//         model: 'Ford',
//         year: 2009
//
//     },
//         {
//             model: 'Chevy',
//             year: 2021
//         }
//     ]
//
    //insertMany works only with array of objects, insert works with anything
//
//     db.collection('cars').insertMany(cars, (err, res) => {
//         if (err) throw err;
//         console.log(res.ops)
//     })
//
//     client.close()
// })


//getting data

MongoClient.connect(url, (err, client) => {
    const db = client.db()

    //get all
    db.collection('cars').find().toArray().then((data) => console.log(data))

    //skip first
    db.collection('cars').find().skip(1).toArray().then((data) => console.log(data))

    //limit to one
    db.collection('cars').find().limit(1).toArray().then((data) => console.log(data))

    //sort data backwards
    db.collection('cars').find().sort({"_id" : -1}).toArray().then((data) => console.log(data))


    //get by value in db
    db.collection('cars').findOne({model: 'Chevy', year: 2021}).then((data) => console.log(data))

    //or for finding many
    db.collection('cars').find({model: 'Ford'}).toArray().then((data) => console.log(data))

    //or to find and remove fields
    db.collection('cars').findOne({}, {model: 0, year: 0}, (err, data) => {
        console.log(data)
    })


    //deleteMany, deleteOne, findOneAndDelete
    db.collection('cars').deleteMany({model: 'Ford'}).then((doc) => console.log(doc))


    //update
    db.collection('cars').findOneAndUpdate({model: 'Chevy'}, {$set: {year: 1917}}, {returnOriginal: false}, (err, doc) => console.log(doc))






    client.close()
})